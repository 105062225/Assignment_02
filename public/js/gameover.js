var gameoverState = {
    create: function() {
        // Add a background image
        game.add.image(0, 0, 'bg_gameover');

        var scoreLabel = game.add.text(game.width/2, 400,
        'your highest score: ' + game.global.highestFloor, { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0.5, 0.5);

        game.input.keyboard.start();
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start, this);

        //btn
        var uploadOnce = 0;
        var btnSubmmit = document.getElementById('btnSubmmit');
        var typeName = document.getElementById('typeName');
        btnSubmmit.addEventListener('click', function () {
            //var name = typeName.value;
            if (typeName.value != "" && uploadOnce==0) {
                var newScoreRef = firebase.database().ref('/score_list').push();
                newScoreRef.set({
                    playerName: typeName.value,
                    score: game.global.highestFloor
                }).then(function() {
                    uploadOnce = 1;
                    alert("success!");
                }).catch(function(error) {
                    alert(error.message);
            });
                //typeName.value = "";
            } else {
                //alert("Please enter your name!");
            }
        });
    },

    start: function() { //back to menu
        var btn = document.getElementById('btnSubmmit');
        btn.style.visibility = 'hidden';
        var typeName = document.getElementById('typeName');
        typeName.value = "";
        typeName.style.visibility = 'hidden';
        var brd = document.getElementById('leaderboard');
        brd.style.visibility = 'visible';
        game.state.start('menu');
    }
}; 
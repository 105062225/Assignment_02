var loadState = {
    preload: function () {

        // Add a 'loading...' label on the screen
        var loadingLabel = game.add.text(game.width/2, 150,
        'loading...', { font: '30px Arial', fill: '#ffffff' });
        loadingLabel.anchor.setTo(0.5, 0.5);

        // Display the progress bar
        var progressBar = game.add.sprite(game.width/2, 200, 'progressBar');
        progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(progressBar);
        //stairs image
        game.load.spritesheet('player', 'assets/player.png', 32, 32);
        game.load.spritesheet('conveyorRight', 'assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyorLeft', 'assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
        game.load.spritesheet('fake', 'assets/fake.png', 96, 36);
        game.load.spritesheet('heal', 'assets/heal2.png', 96, 36);

        game.load.image('normal', 'assets/normal.png');
        game.load.image('ceiling', 'assets/ceiling.png');
        game.load.image('wall', 'assets/wall.png');
        game.load.image('nails', 'assets/nails.png');
        //other image
        game.load.image('heart', 'assets/heart.png');
        game.load.image('stair', 'assets/stair.png');
        // Load a new asset that we will use in the menu state
        game.load.image('bg_gameover', 'assets/bg_gameover.png', 400, 600);
        game.load.image('bg_play', 'assets/bg_play.png', 400, 600);
        game.load.image('bg_menu', 'assets/bg_menu.png', 400, 600);
        //load music
        game.load.audio('hitSound', ['assets/music/hit.mp3', 'assets/music/hit.ogg']);
        game.load.audio('footstep', ['assets/music/footstep.mp3', 'assets/music/footstep.ogg']);
        game.load.audio('dieSound', ['assets/music/die.mp3', 'assets/music/die.ogg']);
        game.load.audio('jumpSound', ['assets/music/jump.mp3', 'assets/music/jump.ogg']);
        game.load.audio('fakeSound', ['assets/music/fake.mp3', 'assets/music/fake.ogg']);
    },


    create: function() {
        // Go to the menu state
        var btn = document.getElementById('btnSubmmit');
        btn.style.visibility = 'hidden';
        var typeName = document.getElementById('typeName');
        typeName.style.visibility = 'hidden';
        var brd = document.getElementById('leaderboard');
        brd.style.visibility = 'visible';
        game.state.start('menu');
    }
}; 
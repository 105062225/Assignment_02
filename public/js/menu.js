var menuState = {
    create: function() {
        // Add a background image
        game.add.image(0, 0, 'bg_menu');
        // Display the name of the game
            // var nameLabel = game.add.text(game.width/2, 90, 'Leaderboard',
            // { font: '30px Arial', fill: '#ffffff' });
            // nameLabel.anchor.setTo(0.5, 0.5);
        // Show the score at the center of the screen
        var scoreLabel = game.add.text(150, 465,
        'your highest score: ' + game.global.highestFloor, { font: '25px Arial', fill: '#ffffff' });
        scoreLabel.anchor.setTo(0, 0);
        // Explain how to start the game
        // var startLabel = game.add.text(game.width/2, 480,
        // 'press Enter to start', { font: '25px Arial', fill: '#ffffff' });
        // startLabel.anchor.setTo(0.5, 0.5);
        // Create a new Phaser keyboard variable: the up arrow key
        // When pressed, call the 'start'
        var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        enterKey.onDown.add(this.start, this);

        var scoreRef = firebase.database().ref('/score_list');
        var total_post = [];
        var descending = [];
        scoreRef.orderByChild("score").limitToLast(10).once("value")
            .then(function(snapshot){
                var k=0;
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    total_post[k] = ". " + childData.playerName + ": " + childData.score.toString();
                    k+=1;
                });
                for(i=1; i<=total_post.length; i++){
                    descending[i] = i.toString() + total_post[total_post.length-i]+ "<br>";
                }
                document.getElementById('leaderboard').innerHTML = descending.join('');
            })
            .catch(e => console.log(e.message));
    },

    start: function() {
        // Start the actual game
        document.getElementById('btnSubmmit').style.visibility = 'hidden';
        var typeName = document.getElementById('typeName');
        typeName.style.visibility = 'hidden';
        document.getElementById('leaderboard').style.visibility = 'hidden';
        game.state.start('play');
    },
}; 
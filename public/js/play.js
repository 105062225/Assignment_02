var playState = {
    preload: function() {}, // The proload state does nothing now.


    create: function() {
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        //init global var
          //game.global.highestFloor = 0;
          //game.global.health = 10;
        //create cursor and keyboard
        game.input.keyboard.start();
        this.cursor = game.input.keyboard.createCursorKeys();
        this.keyboard = game.input.keyboard.addKeys({
            'enter': Phaser.Keyboard.ENTER,
            'up': Phaser.Keyboard.UP,
            'down': Phaser.Keyboard.DOWN,
            'left': Phaser.Keyboard.LEFT,
            'right': Phaser.Keyboard.RIGHT,
            'a': Phaser.Keyboard.A,
            'd': Phaser.Keyboard.D,
            'r': Phaser.Keyboard.R,
            'p': Phaser.Keyboard.P
        });
        //add bg
        game.add.image(0, 0, 'bg_play');
        game.add.image(20, 20, 'heart');
        game.add.image(300, 20, 'stair');
        //floor, health
        this.floorLabel = game.add.text(335, 20, '0', { font: '25px Arial', fill: '#ffffff' });
        this.healthLabel = game.add.text(40, 30, '10', { font: '18px Arial', fill: '#ffffff' });
        // Initialize the score variable
        this.floor = 0;
        //add player and animation
        this.player = game.add.sprite(game.width/2, game.height/4, 'player');
        this.player.anchor.setTo(0.5, 0.5); // Set anchor to middle of sprite
        game.physics.arcade.enable(this.player);    // Enable Physics
        this.player.enableBody = true;      // Enable Physics
        this.player.body.setSize(22, 32, 5, 0);
        this.player.body.gravity.y = 600; //player gravity
        this.player.isHurt = false;
        this.player.unbeatableTime = 0;
        this.player.touchOn = undefined;
        this.player.health = 10;
        this.player.animations.add('leftwalk', [0, 1, 2, 3], 8, true); //animation
        this.player.animations.add('leftwalk_hurt', [4, 5, 6, 7], 8, false);
        this.player.animations.add('rightwalk', [9, 10, 11, 12], 8, true);
        this.player.animations.add('rightwalk_hurt', [13, 14, 15, 16], 8, false);
        this.player.animations.add('leftfall', [18, 19, 20, 21], 12);
        this.player.animations.add('leftfall_hurt', [22, 23, 24, 25], 12);
        this.player.animations.add('rightfall', [27, 28, 29, 30], 12);
        this.player.animations.add('rightfall_hurt', [31, 32, 33, 34], 12);
        this.player.animations.add('fall', [36, 37, 38, 39], 12);
        this.player.animations.add('fall_hurt', [40, 41, 42, 43], 12);
        //add first stand place
        this.startplate = game.add.sprite(game.width/2, game.height-30, 'normal');
        this.startplate.anchor.setTo(0.5, 0.5); 
        game.physics.arcade.enable(this.startplate);
        this.startplate.enableBody = true;
        this.startplate.body.velocity.y = -100;
        this.startplate.body.immovable = true;
        //add ceiling
        this.ceiling = game.add.sprite(0, 0, 'ceiling');
        game.physics.arcade.enable(this.ceiling); 
        this.ceiling.enableBody = true;
        this.ceiling.body.immovable = true;
        //add walls
        this.walls = game.add.group();
        this.walls.enableBody = true;
        game.add.sprite(0, 0, 'wall', 0, this.walls);
        game.add.sprite(0, 400, 'wall', 0, this.walls);
        game.add.sprite(382, 0, 'wall', 0, this.walls);
        game.add.sprite(382, 400, 'wall', 0, this.walls);
        //game.add.sprite(0, game.height, 'ceiling', 0, this.walls);//debug floor
        this.walls.setAll('body.immovable', true);
        //add stairs
        this.normalStairs = game.add.group(); 
        this.normalStairs.enableBody = true;
        this.normalStairs.createMultiple(10, 'normal');

        this.traps = game.add.group(); 
        this.traps.enableBody = true;
        this.traps.createMultiple(10, 'nails');

        this.leftStairs = game.add.group(); 
        this.leftStairs.enableBody = true;
        this.leftStairs.createMultiple(10, 'conveyorLeft');

        this.rightStairs = game.add.group(); 
        this.rightStairs.enableBody = true;
        this.rightStairs.createMultiple(10, 'conveyorRight');

        this.healStairs = game.add.group(); 
        this.healStairs.enableBody = true;
        this.healStairs.createMultiple(1, 'heal');

        this.healing = game.add.group(); 
        this.healing.enableBody = true;
        this.healing.createMultiple(10, 'heart');

        this.jumpStairs = game.add.group(); 
        this.jumpStairs.enableBody = true;
        this.jumpStairs.createMultiple(10, 'trampoline');
        //create stairs
        game.time.events.loop(800, this.createStair, this);
    },


    update: function() {
        //enable collide
        game.physics.arcade.collide(this.player, this.walls);
        game.physics.arcade.collide(this.player, this.startplate, this.gainHP, null, this);
        //game.physics.arcade.collide(this.player, this.ceiling, this.loseHP, null, this);
        this.checkTouchCeiling(this.player);
        game.physics.arcade.collide(this.player, this.normalStairs, this.gainHP, null, this);
        game.physics.arcade.collide(this.player, this.traps, this.loseHP, null, this);
        game.physics.arcade.collide(this.player, this.leftStairs, this.shiftLeft, null, this);
        game.physics.arcade.collide(this.player, this.rightStairs, this.shiftRight, null, this);
        //game.physics.arcade.overlap(this.player, this.healStairs, this.heal, null, this);
        game.physics.arcade.overlap(this.player, this.healing, this.heal, null, this);
        game.physics.arcade.collide(this.player, this.jumpStairs, this.jump, null, this);
        //control player
        this.movePlayer();
        //update floor and health
        this.updateScore();
        //check if player die
        if (this.player.body.y>game.height) {
            var dieSound = game.add.audio('dieSound');//play sound
            dieSound.play();
            this.gameOver();
        }
        if (this.player.health<=0) { this.gameOver();}
        if (this.keyboard.r.isDown) { this.returnMenu();}
        //pause
        window.onkeydown = function(event) {  if (event.keyCode == 80){       game.paused = !game.paused;   } }
        // if(this.keyboard.p.isDown){
        //     if(!this.game.paused) this.game.paused = true;
        //     else this.game.paused = false; 
        // }
        
    }, 

    movePlayer: function() {
        if (this.cursor.left.isDown || this.keyboard.a.isDown) {
            this.player.body.velocity.x = -250;
            if(!this.player.body.touching.down){
                if(!this.player.isHurt) this.player.animations.play('leftfall');
                else {
                    this.player.animations.play('leftfall_hurt');
                    this.player.isHurt = false;
                }
            }
            else{
                if(!this.player.isHurt) this.player.animations.play('leftwalk');
                else {
                    this.player.animations.play('leftwalk_hurt');
                    this.player.isHurt = false;
                }
            }

        }
        else if (this.cursor.right.isDown || this.keyboard.d.isDown) {
            this.player.body.velocity.x = 250;
            if(!this.player.body.touching.down){
                if(!this.player.isHurt) this.player.animations.play('rightfall');
                else {
                    this.player.animations.play('rightfall_hurt');
                    this.player.isHurt = false;
                }
            }
            else{
                if(!this.player.isHurt) this.player.animations.play('rightwalk');
                else {
                    this.player.animations.play('rightwalk_hurt');
                    this.player.isHurt = false;
                }
            }
        }
        else {
            // Stop the player 
            this.player.body.velocity.x = 0;

            if(!this.player.body.touching.down){
                if(!this.player.isHurt) this.player.animations.play('fall');
                else {
                    this.player.animations.play('fall_hurt');
                    this.player.isHurt = false;
                }
            }
            else{
                if(this.player.isHurt) {
                    this.player.frame = 17;
                }else {
                    this.player.frame = 8;
                }
                // Stop the animation
                this.player.animations.stop();
            }
            
        }   
    },

    createStair: function() {
        this.floor += 1;
        var stair;
        var x = Math.random()*(400 - 96 - 40) + 20;
        var y = game.height;
        var rand = Math.random() * 85;
    
        if(rand < 20) {
            stair = this.jumpStairs.getFirstDead(); //jump 20
            if (!stair) { return;}
            stair.frame = 2;
            stair.animations.add('jump', [2, 3, 4, 5, 4, 3, 2, 1, 0, 1, 2], 120);

        } else if (rand < 40) {
            stair = this.traps.getFirstDead(); //trap 20
            if (!stair) { return;}
            stair.body.setSize(96, 1, 0, 15);
        } else if (rand < 50) {
            stair = this.leftStairs.getFirstDead(); //left
            if (!stair) { return;}
            stair.animations.add('shift', [0, 1, 2, 3], 16, true);
            stair.play('shift');
        } else if (rand < 60) {
            stair = this.rightStairs.getFirstDead(); //right 20
            if (!stair) { return;}
            stair.animations.add('shift', [0, 1, 2, 3], 16, true);
            stair.play('shift');
        } else if (rand < 65) {
            // stair = this.healStairs.getFirstDead(); // heal 5
            // stair.animations.add('fade', [0, 1, 2, 3, 4, 5], 18, false);
            // stair.body.setSize(96, 15, 0, 15);
            stair = this.healing.getFirstDead();
            if (!stair) { return;}
        } else {
            stair = this.normalStairs.getFirstDead(); //normal 20
        }
        stair.reset(x, y);
        if(this.floor<30)stair.body.velocity.y = -100;
        else if(this.floor<60)stair.body.velocity.y = -110;
        else if(this.floor<90)stair.body.velocity.y = -120;
        else stair.body.velocity.y = -130;
        stair.body.immovable = true;
        stair.checkWorldBounds = true;
        stair.outOfBoundsKill = true;

        stair.body.checkCollision.down = false;
        stair.body.checkCollision.left = false;
        stair.body.checkCollision.right = false;
    },

    updateScore: function() {
        this.floorLabel.text = this.floor;
        this.healthLabel.text = this.player.health;
    },
    loseHP: function(player, stair) {
        
        this.player.isHurt = true;
        if (this.player.touchOn !== stair) {
            game.camera.flash(0xff0000, 100);
            if(this.player.health > 0) this.player.health -= 3;
            this.player.touchOn = stair;
            var hitSound = game.add.audio('hitSound'); //play sound
            hitSound.play();
        }
    },
    gainHP: function(player, stair) {
        
        if (this.player.touchOn !== stair) {
            var footstep = game.add.audio('footstep'); //play sound
            footstep.play();
            if(this.player.health < 10) this.player.health += 1;
            this.player.touchOn = stair;
            
        }
    },
    jump: function(player, stair){
        var jumpSound = game.add.audio('jumpSound');//play sound
        jumpSound.play();
        this.player.body.velocity.y = -300;
        stair.animations.play('jump');
        //if(this.player.health < 10) this.player.health += 1;
    },
    shiftLeft: function(player, stair){
        this.player.body.x -= 2;
        if (this.player.touchOn !== stair) {
            var footstep = game.add.audio('footstep'); //play sound
            footstep.play();
            this.player.touchOn = stair;
        }
    },
    shiftRight: function(player, stair){
        this.player.body.x += 2;
        if (this.player.touchOn !== stair) {
            var footstep = game.add.audio('footstep'); //play sound
            footstep.play();
            this.player.touchOn = stair;
        }
    },

    heal: function(player, stair){
        stair.kill();
        if(this.player.health < 7) this.player.health += 3;
        else this.player.health = 10;
        // if(this.player.touchOn != stair) {
        //     var fakeSound = game.add.audio('fakeSound');//play sound
        //     fakeSound.play();
        //     stair.animations.play('turn');

        //     if(this.player.health < 9) this.player.health += 2;
        //     else if(this.player.health == 9) this.player.health = 10;
        //     this.player.body.velocity.y = 0;
        //     setTimeout(function() {stair.body.checkCollision.up = false;}, 300);

        //     player.touchOn = stair;
        // }
    },
    checkTouchCeiling: function(player) {
        if(this.player.body.y < 0) {
            this.player.body.y = 10;
            if(this.player.body.velocity.y < 0) {
                this.player.body.velocity.y = 0;
            }
            if(game.time.now > this.player.unbeatableTime) {
                var hitSound = game.add.audio('hitSound'); //play sound
                hitSound.play();
                this.player.health -= 3;
                game.camera.flash(0xff0000, 100);
                this.player.unbeatableTime = game.time.now + 2000;
            }
        }
    },
    gameOver: function() {
        var btn = document.getElementById('btnSubmmit');
        btn.style.visibility = 'visible';
        var typeName = document.getElementById('typeName');
        typeName.style.visibility = 'visible';
        var brd = document.getElementById('leaderboard');
        brd.style.visibility = 'hidden';
        game.input.keyboard.stop();

        if(this.floor>game.global.highestFloor) game.global.highestFloor = this.floor;
        game.state.start('gameover');
    },
    returnMenu: function() {
        var btn = document.getElementById('btnSubmmit');
        btn.style.visibility = 'hidden';
        var typeName = document.getElementById('typeName');
        typeName.style.visibility = 'hidden';
        var brd = document.getElementById('leaderboard');
        brd.style.visibility = 'visible';
        if(this.floor>game.global.highestFloor) game.global.highestFloor = this.floor;
        game.state.start('menu');
        
    },
};

